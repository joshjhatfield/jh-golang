package main

import (
	"database/sql"
	"fmt"
)

func main()  {

	db, err := sql.Open("mysql", "root:root@tcp(localhost:3306)/employees?charset=utf8")
	checkErr(err)


	rows, err := db.Query("SELECT first_name FROM employees WHERE emp_no='10001'")
	checkErr(err)

	for rows.Next(){
		var first_name string
		err = rows.Scan(first_name)

		fmt.Println(first_name)

	}


}
