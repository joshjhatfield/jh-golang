package main

import (
	"fmt"
)






func main()  {

	age := 29

	var pi float64 = 3.1345

	var name string = "Josh"



	fmt.Printf("%s is %d and pi is %f maybe\n", name, age, pi)

	/*
	generic printing stuff AND  bools
	 */

	const piagain float64  =  3.143453

	fmt.Println(name + " Wrote this" )

	var IsUnder40 bool = true

	fmt.Printf("That he is under 40 is %t\n", IsUnder40)

	fmt.Println("true && false =", true && false)
	fmt.Println("true || false =", true || false)
	fmt.Println("!true =", !true)


	/*
	A for loop
	 */


	i :=1 // := auto figures out the var operator type .eg string, float64 etc etc.

	for i <= 3 {
		fmt.Println(i)
		i++   // this = i = i + 1 very cool !!
	}

	/*
	An if / then statement
	 */

	 if age >= 19 {
	 	fmt.Println("he is not a teenager")
	 } else if  age >= 50 {
	 	fmt.Println("he is getting too old for this shit")
	 } else {
	 	fmt.Println("he is middle aged")
	 }

	 // A case switch

	switch age {
	case 21: fmt.Println("have a party")
	case 30: fmt.Println("have your 30th")
	case 100: fmt.Println("you should be dead!")
	default: fmt.Println("no need for celebration")

	}

}
